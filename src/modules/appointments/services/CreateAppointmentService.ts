import { startOfHour } from 'date-fns';
// import { getCustomRepository } from 'typeorm';
import AppError from '@shared/errors/AppError';
import { injectable, inject } from 'tsyringe';
import Appointment from '../infra/typeorm/entities/Appointment';
// import AppointmentsRepository from '../infra/typeorm/repositories/AppointmentsRepository';
import IAppointmentsRepository from '../repositories/IAppointmentsRepository';
/*
 [x] Recebimento das informações
 [/] Tratativa de erros/exessões
 [x] Acesso ao repositório
*/

interface IRequest {
  provider_id: string;
  date: Date;
}
/**
 * Dependency Inversion
 * sempre que o serve tiver uma dependencia experna
 * ele recebe ela como parametro no constructor
 * para os serviços usarem a mesma instância
 */

@injectable()
class CreateAppointmentService {
  // SOLID, 'D' inversion dependence
  constructor(
    @inject('AppointmentsRepository')
    private appointmentsRepository: IAppointmentsRepository,
  ) {}

  public async execute({ date, provider_id }: IRequest): Promise<Appointment> {
    // const appointmentsRepository = getCustomRepository(AppointmentsRepository);

    const appointmentDate = startOfHour(date);

    const findAppointmentInSameDate = await this.appointmentsRepository.findByDate(
      appointmentDate,
    );

    if (findAppointmentInSameDate) {
      throw new AppError('This appointment is already booked');
    }

    const appointment = await this.appointmentsRepository.create({
      provider_id,
      date: appointmentDate,
    });

    // await appointmentsRepository.save(appointment);
    return appointment;
  }
}

export default CreateAppointmentService;
